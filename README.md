
# Come One, Come All!

# To The Anon Student Collaboration of WJB2010

###  Here you can find art, project, and fun suprises.

### Feel Free to Click Around To See What You Can Find!

*Installments*

* [Art Installments](art/README.md "My Art README file")
    * Ceci Est Le Prof
    * Rotating Exhibit

* [Worship](monolith/README.md "My Monolith README file")
    * An Introduction to the Monolith project
    * Carter-anity

### How many days does Jowett struggle with the WJB2010 computer?
    * 3/3 days since the first day of school (updated 1/12/2022 )
